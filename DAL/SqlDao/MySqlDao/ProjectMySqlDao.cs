using System.Collections.Generic;
using DAL.SqlDao.MySqlDao.Utils;
using Logic.Interfaces;
using Logic.Model;
using MySql.Data.MySqlClient;

namespace DAL.SqlDao.MySqlDao
{
    public class ProjectMySqlDao : SqlDao, IProjectDao
    {
        public ProjectMySqlDao(ISqlConnectionFactory sqlConnectionFactory) : base(sqlConnectionFactory)
        {
        }

        public bool AddProject(int organizationId, string projectName)
        {
            using (MySqlConnection connection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText =
                        "INSERT INTO projects(organization_id, name) VALUES (@organizationId, @projectName)";
                    cmd.Parameters.AddWithValue("organizationId", organizationId);
                    cmd.Parameters.AddWithValue("projectName", projectName);
                    connection.Open();
                    return cmd.ExecuteNonQuery() > 0;
                }
            }
        }

        public List<Project> GetProjectsByOrganization(int organizationId)
        {
            List<Project> projects = new List<Project>();
            using (MySqlConnection mySqlConnection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (MySqlCommand cmd = mySqlConnection.CreateCommand())
                {
                    cmd.CommandText =
                        "SELECT projects.* FROM projects WHERE projects.organization_id = @organizationId";
                    cmd.Parameters.AddWithValue("organizationId", organizationId);
                    mySqlConnection.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            projects.Add(new Project
                            {
                                Id = reader.GetInt32("project_id"),
                                OrganizationId = reader.GetInt32("organization_id"),
                                Name = reader.GetStringOrNull("name")
                            });
                        }
                    }
                }
            }

            return projects;
        }

        public bool Create(Project entity)
        {
            throw new System.NotImplementedException();
        }

        public Project GetById(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Update(Project entity)
        {
            throw new System.NotImplementedException();
        }

        public bool Delete(Project entity)
        {
            throw new System.NotImplementedException();
        }
    }
}