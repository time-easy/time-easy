using System;
using System.Collections.Generic;
using DAL.SqlDao.MySqlDao.Utils;
using Logic.DTO;
using Logic.Interfaces;
using Logic.Model;
using MySql.Data.MySqlClient;

namespace DAL.SqlDao.MySqlDao
{
    public class OrganizationMySqlDao : SqlDao, IOrganizationDao
    {
        public OrganizationMySqlDao(ISqlConnectionFactory sqlConnectionFactory) : base(sqlConnectionFactory)
        {
        }

        public bool Create(Organization organization)
        {
            using (MySqlConnection connection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText =
                        "INSERT INTO organizations(name) VALUES (@name)";
                    cmd.Parameters.AddWithValue("name", organization.Name);
                    connection.Open();
                    return cmd.ExecuteNonQuery() > 0;
                }
            }
        }

        public Organization GetById(int organizationId)
        {
            using (MySqlConnection connection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM organizations WHERE organization_id = @organizationId";
                    cmd.Parameters.AddWithValue("organizationId", organizationId);
                    connection.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new Organization
                            {
                                Id = reader.GetInt32("organization_id"),
                                Name = reader.GetStringOrNull("name"),
                                Logo = reader.GetStringOrNull("logo")
                            };
                        }

                        throw new InvalidOperationException();
                    }
                }
            }
        }

        public bool Update(Organization entity)
        {
            throw new System.NotImplementedException();
        }

        public bool Delete(Organization entity)
        {
            throw new System.NotImplementedException();
        }

        public List<Organization> GetOrganizationsByMember(int userId)
        {
            List<Organization> organizations = new List<Organization>();
            using (MySqlConnection mySqlConnection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (MySqlCommand cmd = mySqlConnection.CreateCommand())
                {
                    cmd.CommandText =
                        "SELECT organizations.* FROM organizations " +
                        "INNER JOIN organization_members " +
                        "ON organizations.organization_id = organization_members.organization_id " +
                        "WHERE organization_members.user_id = @userId";
                    cmd.Parameters.AddWithValue("userId", userId);
                    mySqlConnection.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            organizations.Add(new Organization
                            {
                                Id = reader.GetInt32("organization_id"),
                                Name = reader.GetStringOrNull("name"),
                                Logo = reader.GetStringOrNull("logo")
                            });
                        }
                    }
                }
            }

            return organizations;
        }

        public bool CreateOrganizationWithAdmin(Organization organization, int userId)
        {
            using (MySqlConnection connection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "CALL createOrganization(@userId, @organizationName)";
                    cmd.Parameters.AddWithValue("organizationName", organization.Name);
                    cmd.Parameters.AddWithValue("userId", userId);
                    connection.Open();
                    return cmd.ExecuteNonQuery() > 0;
                }
            }
        }

        public IEnumerable<OrganizationMemberDetails> GetMembersByOrganizationId(int organizationId)
        {
            List<OrganizationMemberDetails> organizationMembers = new List<OrganizationMemberDetails>();
            using (MySqlConnection connection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText =
                        "SELECT users.user_id, users.name, users.email, organization_members.is_admin FROM" + 
                        " organization_members INNER JOIN users ON organization_members.user_id = users.user_id WHERE " + 
                        "organization_members.organization_id = @organizationId";
                    cmd.Parameters.AddWithValue("organizationId", organizationId);
                    connection.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            organizationMembers.Add(new OrganizationMemberDetails
                            {
                                Email = reader.GetStringOrNull("email"),
                                Id = reader.GetInt32("user_id"),
                                IsAdmin = reader.GetBoolean("is_admin"),
                                Name = reader.GetStringOrNull("name")
                            });
                        }
                    }
                }
            }

            return organizationMembers;
        }

        public bool AddMemberToOrganization(int userId, int organizationId)
        {
            using (MySqlConnection connection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText =
                        "INSERT INTO organization_members(user_id, organization_id) VALUES (@userId, @organizationId)";
                    cmd.Parameters.AddWithValue("userId", userId);
                    cmd.Parameters.AddWithValue("organizationId", organizationId);
                    connection.Open();
                    return cmd.ExecuteNonQuery() > 0;
                }
            }
        }
    }
}