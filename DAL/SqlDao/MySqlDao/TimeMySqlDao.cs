using System;
using System.Collections.Generic;
using DAL.SqlDao.MySqlDao.Utils;
using Logic.Interfaces;
using Logic.Model;
using MySql.Data.MySqlClient;

namespace DAL.SqlDao.MySqlDao
{
    public class TimeMySqlDao : SqlDao, ITimeDao
    {
        public TimeMySqlDao(ISqlConnectionFactory sqlConnectionFactory) : base(sqlConnectionFactory)
        {
        }

        public bool Create(TimeEntry timeEntry)
        {
            using (MySqlConnection connection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText =
                        "INSERT INTO time_registrations(project_id, user_id, date, time, comment) VALUES (@projectId, @userId, @date, @time, @comment)";
                    cmd.Parameters.AddWithValue("projectId", timeEntry.ProjectId);
                    cmd.Parameters.AddWithValue("userId", timeEntry.UserId);
                    cmd.Parameters.AddWithValue("date", timeEntry.Date);
                    cmd.Parameters.AddWithValue("time", timeEntry.Time);
                    cmd.Parameters.AddWithValue("comment", timeEntry.Comment);
                    connection.Open();
                    return cmd.ExecuteNonQuery() > 0;
                }
            }
        }

        public TimeEntry GetById(int id)
        {
            throw new System.NotImplementedException();
        }

        public bool Update(TimeEntry entity)
        {
            using (MySqlConnection connection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText =
                        "UPDATE time_registrations SET time = @time WHERE time_registration_id = @timeRegistrationId";
                    cmd.Parameters.AddWithValue("time", entity.Time);
                    cmd.Parameters.AddWithValue("timeRegistrationId", entity.Id);
                    connection.Open();
                    return cmd.ExecuteNonQuery() > 0;
                }
            }
        }

        public bool Delete(TimeEntry entity)
        {
            throw new System.NotImplementedException();
        }

        public List<TimeEntry> GetByUserAndDateAndOrganization(int userId, DateTime date, int organizationId)
        {
            List<TimeEntry> timeEntries = new List<TimeEntry>();
            using (MySqlConnection mySqlConnection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (MySqlCommand cmd = mySqlConnection.CreateCommand())
                {
                    cmd.CommandText =
                        "SELECT time_registrations.* FROM time_registrations " +
                        "INNER JOIN time_easy.projects ON time_registrations.project_id = projects.project_id " +
                        "WHERE time_registrations.date = @date AND time_easy.projects.organization_id = @organizationId AND time_easy.time_registrations.user_id = @userId";
                    cmd.Parameters.AddWithValue("organizationId", organizationId);
                    cmd.Parameters.AddWithValue("date", date);
                    cmd.Parameters.AddWithValue("userId", userId);
                    mySqlConnection.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            timeEntries.Add(new TimeEntry
                            {
                                Id = reader.GetInt32("time_registration_id"),
                                UserId = reader.GetInt32("user_id"),
                                Comment = reader.GetStringOrNull("comment"),
                                Date = reader.GetDateTime("date"),
                                ProjectId = reader.GetInt32("project_id"),
                                Time = reader.GetDecimal("time")
                            });
                        }
                    }
                }
            }

            return timeEntries;
        }

        public List<TimeEntry> GetByUserAndDateAndProject(int userId, DateTime date, int projectId)
        {
            List<TimeEntry> timeEntries = new List<TimeEntry>();
            using (MySqlConnection mySqlConnection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (MySqlCommand cmd = mySqlConnection.CreateCommand())
                {
                    cmd.CommandText =
                        "SELECT time_registrations.* FROM time_registrations " +
                        "WHERE time_registrations.date = @date AND time_registrations.user_id = @userId AND time_registrations.project_id = @projectId";
                    cmd.Parameters.AddWithValue("projectId", projectId);
                    cmd.Parameters.AddWithValue("date", date);
                    cmd.Parameters.AddWithValue("userId", userId);
                    mySqlConnection.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            timeEntries.Add(new TimeEntry
                            {
                                Id = reader.GetInt32("time_registration_id"),
                                UserId = reader.GetInt32("user_id"),
                                Comment = reader.GetStringOrNull("comment"),
                                Date = reader.GetDateTime("date"),
                                ProjectId = reader.GetInt32("project_id"),
                                Time = reader.GetDecimal("time")
                            });
                        }
                    }
                }
            }

            return timeEntries;
        }

        public List<TimeEntry> GetAllTimeEntriesByUserAndOrganization(int userId, int organizationId)
        {
            List<TimeEntry> timeEntries = new List<TimeEntry>();
            using (MySqlConnection mySqlConnection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (MySqlCommand cmd = mySqlConnection.CreateCommand())
                {
                    cmd.CommandText =
                        "SELECT time_registrations.* FROM time_registrations " +
                        "INNER JOIN time_easy.projects ON time_registrations.project_id = projects.project_id " +
                        "WHERE time_easy.projects.organization_id = @organizationId AND time_easy.time_registrations.user_id = @userId";
                    cmd.Parameters.AddWithValue("organizationId", organizationId);
                    cmd.Parameters.AddWithValue("userId", userId);
                    mySqlConnection.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            timeEntries.Add(new TimeEntry
                            {
                                Id = reader.GetInt32("time_registration_id"),
                                UserId = reader.GetInt32("user_id"),
                                Comment = reader.GetStringOrNull("comment"),
                                Date = reader.GetDateTime("date"),
                                ProjectId = reader.GetInt32("project_id"),
                                Time = reader.GetDecimal("time")
                            });
                        }
                    }
                }
            }

            return timeEntries;
        }
    }
}