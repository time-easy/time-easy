using System;
using System.Collections.Generic;
using DAL.SqlDao.MySqlDao.Utils;
using MySql.Data.MySqlClient;
using Logic.Interfaces;
using Logic.Model;

namespace DAL.SqlDao.MySqlDao
{
    public class UserMySqlDao : SqlDao, IUserDao
    {

        public UserMySqlDao(ISqlConnectionFactory sqlConnectionFactory) : base(sqlConnectionFactory)
        {
        }

        public IEnumerable<User> FindByEmailAddress(string email)
        {
            List<User> result = new List<User>();
            using (MySqlConnection connection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM users WHERE email = @emailAddress";
                    cmd.Parameters.AddWithValue("emailAddress", email);
                    connection.Open();
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(new User
                            {
                                Id = reader.GetInt32("user_id"),
                                Email = reader.GetStringOrNull("email"),
                                Active = reader.GetBoolean("active"),
                                Name = reader.GetStringOrNull("name"),
                                Password = reader.GetStringOrNull("password")
                            });
                        }
                    }
                }
            }

            return result;
        }

        public bool DoesUserExist(int userId)
        {
            using (MySqlConnection connection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT COUNT(*) AS usercount FROM users WHERE user_id = @userId";
                    cmd.Parameters.AddWithValue("userId", userId);
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        return reader.GetInt32("usercount") > 0;
                    }

                    throw new InvalidOperationException();
                }
            }
        }

        public bool Create(User entity)
        {
            using (MySqlConnection connection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText =
                        "INSERT INTO users(email, password, name, active) VALUES (@emailAddress, @password, @name, 0)";
                    cmd.Parameters.AddWithValue("emailAddress", entity.Email);
                    cmd.Parameters.AddWithValue("password", entity.Password);
                    cmd.Parameters.AddWithValue("name", entity.Name);
                    connection.Open();
                    return cmd.ExecuteNonQuery() > 0;
                }
            }
        }

        public User GetById(int userId)
        {
            using (MySqlConnection connection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM users WHERE user_id = @userId";
                    cmd.Parameters.AddWithValue("userId", userId);
                    connection.Open();
                    var reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        return new User
                        {
                            Id = reader.GetInt32("user_id"),
                            Email = reader.GetString("email"),
                            Name = reader.GetStringOrNull("name"),
                            Active = reader.GetBoolean("active"),
                            Password = reader.GetString("password")
                        };
                    }

                    throw new InvalidOperationException();
                }
            }
        }

        public bool Update(User entity)
        {
            using (MySqlConnection connection = (MySqlConnection) SqlConnectionFactory.CreateConnection())
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "UPDATE users SET ";
                    if (entity.Email != null)
                    {
                        cmd.CommandText += " email = @emailAddress ";
                        cmd.Parameters.AddWithValue("emailAddress", entity.Email);
                    }

                    if (entity.Name != null)
                    {
                        cmd.CommandText += " name = @name ";
                        cmd.Parameters.AddWithValue("name", entity.Name);
                    }

                    if (entity.Password != null)
                    {
                        cmd.CommandText += " password = @password ";
                        cmd.Parameters.AddWithValue("password", entity.Password);
                    }

                    if (entity.Active != null)
                    {
                        cmd.CommandText += " active = @active ";
                        cmd.Parameters.AddWithValue("active", entity.Active);
                    }

                    cmd.CommandText += " WHERE user_id = @userId";
                    cmd.Parameters.AddWithValue("userId", entity.Id);
                    connection.Open();
                    return cmd.ExecuteNonQuery() > 0;

                }
            }
        }

        public bool Delete(User entity)
        {
            throw new NotImplementedException();
        }

    }
}