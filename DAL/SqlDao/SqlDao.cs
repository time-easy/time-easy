using Logic.Interfaces;

namespace DAL.SqlDao
{
    public abstract class SqlDao
    {
        private protected readonly ISqlConnectionFactory SqlConnectionFactory;

        protected SqlDao(ISqlConnectionFactory sqlConnectionFactory)
        {
            SqlConnectionFactory = sqlConnectionFactory;
        }
    }
}