using Logic.Interfaces;
using System.Data.Common;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace DAL.Utils
{
    public class SqlConnectionFactory : ISqlConnectionFactory
    {

        private readonly string _connectionString;
        
        public SqlConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public DbConnection CreateConnection()
        {
            return new MySqlConnection(_connectionString);
        }
    }
}