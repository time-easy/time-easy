create table projects
(
	project_id int auto_increment
		primary key,
	organization_id int null,
	name varchar(255) null,
	constraint projects_organizations_organization_id_fk
		foreign key (organization_id) references organizations (organization_id)
);

