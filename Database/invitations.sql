create table invitations
(
	invitation_id int auto_increment
		primary key,
	email_address varchar(255) not null,
	organization_id int not null,
	is_claimed tinyint(1) default 0 null,
	constraint invitations_organizations_organization_id_fk
		foreign key (invitation_id) references organizations (organization_id)
);

