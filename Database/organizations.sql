create table organizations
(
	organization_id int auto_increment
		primary key,
	name varchar(255) null,
	logo varchar(255) null
);

