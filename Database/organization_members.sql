create table organization_members
(
	user_id int not null,
	organization_id int not null,
	is_admin tinyint(1) default 0 null,
	primary key (user_id, organization_id),
	constraint organization_members_organizations_organization_id_fk
		foreign key (organization_id) references organizations (organization_id),
	constraint organization_members_users_user_id_fk
		foreign key (user_id) references users (user_id)
);

