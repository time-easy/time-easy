create table users
(
	user_id int auto_increment
		primary key,
	email varchar(255) not null,
	name varchar(255) null,
	password varchar(255) not null,
	active tinyint(1) default 0 not null
);

