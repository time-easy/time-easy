create table time_registrations
(
	time_registration_id int auto_increment
		primary key,
	project_id int null,
	user_id int null,
	time decimal(2,1) default 0.0 null,
	comment varchar(255) null,
	constraint time_registrations_projects_project_id_fk
		foreign key (project_id) references projects (project_id),
	constraint time_registrations_users_user_id_fk
		foreign key (user_id) references users (user_id)
);

