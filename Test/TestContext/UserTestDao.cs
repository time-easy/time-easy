using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Logic.Interfaces;
using Logic.Model;

namespace Tests.Context
{
    public class UserTestDao : IUserDao
    {

        private IEnumerable<User> _userCollection = new List<User>();
        
        public IEnumerable<User> FindAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> FindByCondition(Expression<Func<User, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public bool Create(User entity)
        {
            throw new NotImplementedException();
        }

        public User GetById(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(User entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(User entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> FindByEmailAddress(string emailAddress)
        {
            throw new NotImplementedException();
        }
    }
}