using DAL.SqlDao.MySqlDao;
using DAL.Utils;
using Logic.Interfaces;
using Logic.Model;
using MySql.Data.MySqlClient;
using NUnit.Framework;

namespace Tests.IntegrationTests
{
    [TestFixture]
    public class UserMySqlTest
    {
        private IUserDao _userDao;

        [SetUp]
        public void Init()
        {
            var mySqlClientFactory = new SqlConnectionFactory("Server=localhost;Database=time_easy_test;Uid=stef;Pwd=;");
            _userDao = new UserMySqlDao(mySqlClientFactory);
        }
        
        [Test]
        public void Create_ValidUserEntity_InsertsNewUser()
        {
            var user = new User
            {
                Email = "harrie@sunweb.nl",
                Name = "Harrie van de Sunweb",
                Password = "test"
            };

            Assert.IsTrue(_userDao.Create(user));
        }

        [Test]
        public void Create_InvalidUserEntity_ThrowsMySqlException()
        {
            var user = new User();
            
            Assert.Throws<MySqlException>(() => _userDao.Create(user));
        }

        [Test]
        public void Create_InvalidConnection_ThrowsMySqlException()
        {
            var user = new User
            {
                Email = "harrie@sunweb.nl",
                Name = "Harrie van de Sunweb",
                Password = "test"
            };
            
            var tempUserDao = new UserMySqlDao(new SqlConnectionFactory(""));

            Assert.Throws<MySqlException>(() => tempUserDao.Create(user));
        }

        [Test]
        public void FindByEmailAddress_OneMatchinUser_ReturnsIEnumerableOfUsers()
        {
            string searchTerm = "harrie@sunweb.nl";
            
        }
    }
}