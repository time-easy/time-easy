using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Logic.DTO;
using Logic.Exceptions;
using Logic.Interfaces;
using Logic.Model;
using Logic.Utils;

namespace Logic
{
    public class OrganizationLogic
    {
        private readonly IOrganizationDao _organizationDao;
        private readonly IUserDao _userDao;

        public OrganizationLogic(IOrganizationDao organizationDao, IUserDao userDao)
        {
            this._organizationDao = organizationDao;
            this._userDao = userDao;
        }

        public IEnumerable<Organization> GetOrganizationsByMember(int userId)
        {
            return this._organizationDao.GetOrganizationsByMember(userId);
        }

        public void CreateOrganizationWithAdmin(int userId, Organization organization)
        {
            if (string.IsNullOrEmpty(organization.Name))
            {
                throw new OrganizationNameNullOrEmptyException();
            }
            
            //TODO: check if users exists before creating the organization

            if (!_organizationDao.CreateOrganizationWithAdmin(organization, userId))
            {
                throw new Exception("Something went wrong");
            }
        }

        public Organization GetOrganization(int organizationId)
        {
            return this._organizationDao.GetById(organizationId);
        }
        
        public IEnumerable<OrganizationMemberDetails> GetMembersByOrganizationId(int organizationId)
        {
            return this._organizationDao.GetMembersByOrganizationId(organizationId);
        }

        public void InviteUser(int organizationId, string email, JwtGenerator jwtGenerator, IEmailClient emailClient)
        {
            var claims = new List<Claim>
            {
                new Claim("email", email),
                new Claim("organization", organizationId.ToString())
            };

            IEnumerable<OrganizationMemberDetails> matchingUsers = _organizationDao.GetMembersByOrganizationId(organizationId)
                .Where(x => x.Email == email);
            if (matchingUsers.Any())
            {
                throw new UserAlreadyInOrganizationException();
            }

            IEnumerable<User> existingUser = _userDao.FindByEmailAddress(email);
            if (existingUser.Any())
            {
                if (!_organizationDao.AddMemberToOrganization(existingUser.First().Id, organizationId))
                {
                    throw new Exception("Something went wrong");
                }
                return;
            }
            Organization organization = _organizationDao.GetById(organizationId);
            string token = jwtGenerator.GenerateToken(new ClaimsIdentity(claims));
               
            emailClient.SendMail(email, "Please register on Time-Easy",
                $"Hello employee of {organization.Name}, <br/>" +
                $"{organization.Name} is using Time-Easy to register worked time. Please register on Time-Easy to start registering your time. <br/>" +
                $"<a href='http://localhost:8010/register/{token}'>Register</a>");
        }
    }
}