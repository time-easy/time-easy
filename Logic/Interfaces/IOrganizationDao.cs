using System.Collections.Generic;
using Logic.DTO;
using Logic.Model;

namespace Logic.Interfaces
{
    public interface IOrganizationDao : IDao<Organization>
    {
        List<Organization> GetOrganizationsByMember(int userId);

        bool CreateOrganizationWithAdmin(Organization organization, int userId);
        IEnumerable<OrganizationMemberDetails> GetMembersByOrganizationId(int organizationId);
        bool AddMemberToOrganization(int userId, int organizationId);
    }
}