using System.Collections.Generic;
using Logic.Model;

namespace Logic.Interfaces
{
    public interface IProjectDao: IDao<Project>
    {
        bool AddProject(int organizationId, string projectName);
        List<Project> GetProjectsByOrganization(int organizationId);
    }
}