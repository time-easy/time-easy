using System.Collections.Generic;
using Logic.Model;

namespace Logic.Interfaces
{
    public interface IUserDao : IDao<User>
    {
        IEnumerable<User> FindByEmailAddress(string emailAddress);
    }
}