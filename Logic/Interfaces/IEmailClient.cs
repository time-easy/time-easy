namespace Logic.Interfaces
{
    public interface IEmailClient
    {
        void SendMail(string receiver, string subject, string message);
    }
}