namespace Logic.Interfaces
{
    public interface IDao<T> where T : class
    {
        bool Create(T entity);
        T GetById(int id);
        bool Update(T entity);
        bool Delete(T entity);
    }
}