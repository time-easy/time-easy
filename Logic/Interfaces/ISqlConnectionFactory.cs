using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Interfaces
{
    public interface ISqlConnectionFactory
    {
        System.Data.Common.DbConnection CreateConnection();
    }
}
