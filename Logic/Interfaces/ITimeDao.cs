using System;
using System.Collections.Generic;
using Logic.Model;

namespace Logic.Interfaces
{
    public interface ITimeDao: IDao<TimeEntry>
    {
        List<TimeEntry> GetByUserAndDateAndOrganization(int userId, DateTime date, int organizationId);
        List<TimeEntry> GetByUserAndDateAndProject(int userId, DateTime date, int projectId);
        List<TimeEntry> GetAllTimeEntriesByUserAndOrganization(int userId, int organizationId);
    }
}