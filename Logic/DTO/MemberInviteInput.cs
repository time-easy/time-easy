namespace Logic.DTO
{
    public class MemberInviteInput
    {
        public string Email { get; set; }
    }
}