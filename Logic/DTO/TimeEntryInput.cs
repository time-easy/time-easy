using System;

namespace Logic.DTO
{
    public class TimeEntryInput
    {
        public decimal Time { get; set; }
        public string Comment { get; set; }
    }
}