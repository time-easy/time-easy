using Logic.Model;

namespace Logic.DTO
{
    public class OrganizationMemberDetails
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public bool IsAdmin { get; set; }
    }
}