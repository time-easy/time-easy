using System;
using System.Collections;
using System.Collections.Generic;
using Logic.Exceptions;
using Logic.Interfaces;
using Logic.Model;

namespace Logic
{
    public class UserLogic
    {
        private readonly IUserDao _userDao;

        public UserLogic(IUserDao userDao)
        {
            _userDao = userDao;
        }

        public User GetSingleUser(int userId)
        {
            try
            {
                User user = _userDao.GetById(userId);
                user.Password = null;
                return user;
            }

            catch (InvalidOperationException)
            {
                throw new UserNotFoundException();
            }
        }
        
    }
}