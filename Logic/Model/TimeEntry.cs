using System;

namespace Logic.Model
{
    public class TimeEntry
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int UserId { get; set; }
        public DateTime Date { get; set; }
        public decimal Time { get; set; }
        public string Comment { get; set; }
    }
}