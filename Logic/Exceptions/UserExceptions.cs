using System;

namespace Logic.Exceptions
{
    public class UserNotFoundException : Exception
    {
    }

    public class AuthenticationTokenExpiredException : Exception
    {
    }

    public class AuthenticationTokenInvalidException : Exception
    {
    }
}