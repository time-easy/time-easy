using System;

namespace Logic.Exceptions
{
        public class OrganizationNameNullOrEmptyException : Exception
        {
        }

        public class UserAlreadyInOrganizationException : Exception
        {
        }
}