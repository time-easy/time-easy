using System;

namespace Logic.Exceptions
{
    public class InvalidPasswordException : Exception
    {
    }

    public class InvalidEmailException : Exception
    {
    }

    public class EmailAlreadyExistsException : Exception
    {
    }

    public class InvalidUserCredentialsException : Exception
    {
    }
}