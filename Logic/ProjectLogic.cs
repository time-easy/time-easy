using System;
using System.Collections.Generic;
using Logic.Exceptions;
using Logic.Interfaces;
using Logic.Model;

namespace Logic
{
    public class ProjectLogic
    {

        private readonly IProjectDao _projectDao;

        public ProjectLogic(IProjectDao projectDao)
        {
            _projectDao = projectDao;
        }

        public void AddProject(int organizationId, string projectName)
        {
            if (string.IsNullOrEmpty(projectName))
            {
                throw new ProjectNameEmptyOrNullException();
            }
            
            //TODO: Check if the organization does exist.
            //TODO: Check if user has permission to create project.
            
            if (!_projectDao.AddProject(organizationId, projectName))
            {
                throw new Exception("Something went wrong.");
            }
        }

        public List<Project> GetProjectsByOrganization(int organizationId)
        {
            return _projectDao.GetProjectsByOrganization(organizationId);
        }
    }
}