using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Authentication;
using System.Security.Claims;
using Logic.Exceptions;
using Logic.Interfaces;
using Logic.Model;
using Logic.Utils;
using Microsoft.IdentityModel.Tokens;

namespace Logic
{
    public class AuthLogic
    {
        private readonly IUserDao _userDao;

        public AuthLogic(IUserDao userDao)
        {
            _userDao = userDao;
        }

        private static bool IsEmailAddressCorrect(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        public void RegisterNewUser(string email, string name, string password,
            IEmailClient emailClient, JwtGenerator jwtGenerator, string baseUrl)
        {
            if (password.Length < 5)
                throw new InvalidPasswordException();

            if (!IsEmailAddressCorrect(email))
                throw new InvalidEmailException();

            if (_userDao.FindByEmailAddress(email).Any())
                throw new EmailAlreadyExistsException();

            User newUser = new User
            {
                Active = false,
                Email = email,
                Id = 0,
                Name = name,
                Password = BCrypt.Net.BCrypt.HashPassword(password)
            };

            if (!_userDao.Create(newUser))
                throw new Exception("Something went wrong");

            string verificationToken = jwtGenerator.GenerateToken(ClaimTypes.Email, newUser.Email);
               
            emailClient.SendMail(email, "Please confirm your Time-Easy account",
                $"Hey {name}, <br/>" +
                "You have recently registered yourself for Time-Easy. Please click on the link below to activate your account. <br/>" +
                $"<a href='{baseUrl}/api/auth/verify/{verificationToken}'>Verify account</a>");
        }

        public bool VerifyUser(string verifyToken, JwtGenerator jwtGenerator)
        {
            try
            {
                JwtSecurityToken decodedToken = (JwtSecurityToken) jwtGenerator.DecodeJwtToken(verifyToken);
                var claims = decodedToken.Claims;
                string userEmail = decodedToken.Claims.Single(claim => claim.Type == "email").Value;
                User user = _userDao.FindByEmailAddress(userEmail).Single();
                _userDao.Update(new User
                {
                    Id = user.Id,
                    Active = true
                });
            }
            catch (SecurityTokenExpiredException)
            {
                throw new AuthenticationTokenExpiredException();
            }
            catch (SecurityTokenValidationException )
            {
                throw new AuthenticationTokenInvalidException();
            }
            catch (ArgumentException)
            {
                throw new AuthenticationTokenInvalidException();
            }
            catch (InvalidOperationException)
            {
                throw new UserNotFoundException();
            }

            return true;
        }
        
        public string AuthenticateUser(string email, string password, JwtGenerator jwtGenerator)
        {
            IEnumerable<User> searchResult = _userDao.FindByEmailAddress(email).ToList();
            
//            IEnumerable<User> searchResult = _userDao.FindByEmailAddress(email).ToList().Where(user => user.Active = true);
            
            if (!searchResult.Any())
            {
                throw new InvalidUserCredentialsException();
            }

            User userFromDb = searchResult.Single();
            if (!BCrypt.Net.BCrypt.Verify(password, userFromDb.Password))
            {
                throw new InvalidUserCredentialsException();
            }

            return jwtGenerator.GenerateToken(ClaimTypes.Sid, userFromDb.Id.ToString());
        }

    }
}