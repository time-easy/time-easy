using System;
using System.Collections.Generic;
using System.Linq;
using Logic.Interfaces;
using Logic.Model;

namespace Logic
{
    public class TimeLogic
    {
        private readonly ITimeDao _timeDao;

        public TimeLogic(ITimeDao timeDao)
        {
            this._timeDao = timeDao;
        }

        public void RegisterTime(DateTime date, int projectId, int userId, decimal hours, string comment)
        {
            List<TimeEntry> matchingTimeEntries = this._timeDao.GetByUserAndDateAndProject(userId, date, projectId);
            if (matchingTimeEntries.Any())
            {
                if (!this._timeDao.Update(new TimeEntry
                {
                    Id = matchingTimeEntries.First().Id,
                    UserId = matchingTimeEntries.First().UserId,
                    ProjectId = matchingTimeEntries.First().ProjectId,
                    Date = matchingTimeEntries.First().Date,
                    Comment = comment,
                    Time = hours
                }))
                {
                    throw new Exception("Failed to register time.");
                }
                return;
            }
            if (!this._timeDao.Create(new TimeEntry
            {
                Date = date,
                ProjectId = projectId,
                UserId = userId,
                Time = hours,
                Comment = comment
            }))
            {
                throw new Exception("Failed to register time.");
            }
        }

        public List<TimeEntry> GetTimeByUserAndDateAndOrganization(int userId, DateTime date, int organizationId)
        {
            return this._timeDao.GetByUserAndDateAndOrganization(userId, date, organizationId);
        }

        public List<TimeEntry> GetAllTimeEntriesByUserAndOrganization(int userId, int organizationId)
        {
            return this._timeDao.GetAllTimeEntriesByUserAndOrganization(userId, organizationId);
        }
    }
}