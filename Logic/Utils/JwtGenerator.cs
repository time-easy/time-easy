using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;

namespace Logic.Utils
{
    public class JwtGenerator
    {
        private readonly JwtSecurityTokenHandler _jwtSecurityTokenHandler;
        private readonly byte[] _jwtKey;
        
        public JwtGenerator(byte[] jwtKey)
        {
            this._jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            this._jwtKey = jwtKey;
        }

        public string GenerateToken(string type, string value, int validDays)
        {
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(type, value)
                }),
                Expires = DateTime.UtcNow.AddDays(validDays),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(this._jwtKey), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = this._jwtSecurityTokenHandler.CreateToken(tokenDescriptor);
            return this._jwtSecurityTokenHandler.WriteToken(token);
        }
        
        public string GenerateToken(ClaimsIdentity claims)
        {
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = claims,
                Expires = DateTime.Now.AddDays(420),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(this._jwtKey), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = this._jwtSecurityTokenHandler.CreateToken(tokenDescriptor);
            return this._jwtSecurityTokenHandler.WriteToken(token);
        }
        
        public string GenerateToken(string type, string value)
        {
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(type, value)
                }),
                Expires = DateTime.Now.AddDays(420),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(this._jwtKey), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = this._jwtSecurityTokenHandler.CreateToken(tokenDescriptor);
            return this._jwtSecurityTokenHandler.WriteToken(token);
        }

        public SecurityToken DecodeJwtToken(string jwtToken)
        {
            var validations = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(this._jwtKey),
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = false
            };
            this._jwtSecurityTokenHandler.ValidateToken(jwtToken, validations, out SecurityToken validatedToken);
            return validatedToken;
        }
    }
}