using System.Text;
using DAL.SqlDao.MySqlDao;
using Logic.Interfaces;
using Logic.Utils;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using DAL.Utils;
using WebAPI.Utils;

namespace WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            
            IConfigurationSection smtpConfiguration = this.Configuration.GetSection("SMTP");
            EmailClient emailClient = new EmailClient(smtpConfiguration["Host"], 
                int.Parse(smtpConfiguration["Port"]), 
                smtpConfiguration["Username"], 
                smtpConfiguration["Password"]);

            string connectionString = Configuration.GetConnectionString("MySql");
            services.AddTransient<ISqlConnectionFactory>(_ => new SqlConnectionFactory(connectionString));
            services.AddScoped<IUserDao, UserMySqlDao>();
            services.AddScoped<IOrganizationDao, OrganizationMySqlDao>();
            services.AddScoped<IProjectDao, ProjectMySqlDao>();
            services.AddScoped<ITimeDao, TimeMySqlDao>();
            services.AddSingleton<IEmailClient>(emailClient);
            
            byte[] jwtKey = Encoding.ASCII.GetBytes(Configuration["jwtSecret"]);
            services.AddSingleton<JwtGenerator>(new JwtGenerator(jwtKey));
            
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(jwtKey),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.ApplicationServices.GetService<IHttpContextAccessor>();
            
            app.UseHttpsRedirection();

            app.UseAuthentication();
            
            app.UseCors(option => { option.AllowAnyOrigin().AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader(); });
            
            app.UseMvc();
        }
    }
}