using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using Logic;
using Logic.DTO;
using Logic.Exceptions;
using Logic.Interfaces;
using Logic.Model;
using Logic.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Utils;
using WebAPI.Utils.JsendResponse;

namespace WebAPI.Controllers
{
    [Route("api/organizations")]
    [ApiController]
    [Authorize]
    public class OrganizationsController
    {
        private readonly OrganizationLogic _organizationLogic;
        private readonly IEmailClient _emailClient;
        private readonly JwtGenerator _jwtGenerator;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public OrganizationsController(IOrganizationDao organizationDao, JwtGenerator jwtGenerator,
            IEmailClient emailClient, IHttpContextAccessor httpContextAccessor, IUserDao userDao)
        {
            _organizationLogic = new OrganizationLogic(organizationDao, userDao);
            _jwtGenerator = jwtGenerator;
            _emailClient = emailClient;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet("{organizationId:int}")]
        public JsendResponse GetOrganization(int organizationId)
        {
            return new SuccessResponse<Organization>(_organizationLogic.GetOrganization(organizationId));
        }

        [HttpGet("~/api/users/{userId:int}/organizations")]
        public JsendResponse GetOrganizationsByUser(int userId)
        {
            try
            {
                return new SuccessResponse<List<Organization>>(_organizationLogic
                    .GetOrganizationsByMember(userId).ToList());
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return new ErrorResponse("Something went wrong.");
            }
        }

        [HttpPost("~/api/users/{userId:int}/organizations")]
        public JsendResponse CreateOrganization([FromRoute] int userId, [FromBody] Organization organization)
        {
            //TODO: AuthorizationToken's userId should match the userId in URL before creating the organization
            try
            {
                _organizationLogic.CreateOrganizationWithAdmin(userId, organization);
            }
            catch (OrganizationNameNullOrEmptyException)
            {
                return new FailResponse("The organization name was empty.");
            }
            catch (Exception)
            {
                return new ErrorResponse("Something went wrong.");
            }

            return new SuccessResponse<bool>(true);
        }

        [HttpGet("~/api/organization/{organizationId:int}/members")]
        public JsendResponse GetOrganizationMembers([FromRoute] int organizationId)
        {
            return new SuccessResponse<IEnumerable<OrganizationMemberDetails>>(
                _organizationLogic.GetMembersByOrganizationId(organizationId));
        }

        [HttpPost("~/api/organization/{organizationId:int}/members")]
        public JsendResponse InviteMember([FromRoute] int organizationId, [FromBody] MemberInviteInput inviteInput)
        {
            try
            {
                _organizationLogic.InviteUser(organizationId, inviteInput.Email, _jwtGenerator, _emailClient);
            }
            catch (UserAlreadyInOrganizationException)
            {
                return new FailResponse("The user is already in this organization.");
            }
            catch (Exception)
            {
                return new ErrorResponse("Something went wrong.");
            }

            return new SuccessResponse<bool>(true);
        }

//        [HttpDelete("~/api/organization/{organizationId:int}/member/{memberId:int}")]
//        public JsendResponse DeleteOrganizationMembers([FromRoute] int organizationId)
//        {
//            try
//            {
//                _organizationLogic.DeleteMember(organizationId);
//            }
//            return new SuccessResponse<IEnumerable<OrganizationMemberDetails>>(_organizationLogic.GetMembersByOrganizationId(organizationId));
//        }
    }
}