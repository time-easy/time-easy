using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Logic;
using Logic.Exceptions;
using Logic.Interfaces;
using Logic.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Utils.JsendResponse;

namespace WebAPI.Controllers
{
    [Route("api/users")]
    [ApiController]
    [Authorize]
    public class UsersController
    {
        private readonly UserLogic _userLogic;
        
        public UsersController(IUserDao userDao, IOrganizationDao organizationDao)
        {
            _userLogic = new UserLogic(userDao);
        }

        [HttpGet("{userId:int}")]
        public JsendResponse GetUser([FromRoute] int userId)
        {
            try
            {
                return new SuccessResponse<User>(_userLogic.GetSingleUser(userId));
            }
            catch (UserNotFoundException)
            {
                return new FailResponse("The user could not be found");
            }
            catch (Exception e)
            {
                return new ErrorResponse("Something went wrong.");
            }
        }

    }
}