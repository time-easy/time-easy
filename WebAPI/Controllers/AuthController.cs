using System;
using System.Diagnostics;
using Logic;
using Logic.Exceptions;
using Logic.Interfaces;
using Logic.Model;
using Logic.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.DTO;
using WebAPI.Utils.JsendResponse;

namespace WebAPI.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController
    {
        private readonly IUserDao _userDao;
        private readonly IEmailClient _emailClient;
        private readonly JwtGenerator _jwtGenerator;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AuthController(IUserDao userDao, IEmailClient emailClient, JwtGenerator jwtGenerator,
            IHttpContextAccessor httpContextAccessor)
        {
            this._userDao = userDao;
            this._emailClient = emailClient;
            this._jwtGenerator = jwtGenerator;
            this._httpContextAccessor = httpContextAccessor;
        }

        [HttpPost("register")]
        public JsendResponse Register([FromBody] User user)
        {
            var authLogic = new AuthLogic(_userDao);

            var baseUrl = this._httpContextAccessor.HttpContext.Request.Scheme + "://" +
                          this._httpContextAccessor.HttpContext.Request.Host;
            try
            {
                authLogic.RegisterNewUser(user.Email, user.Name, user.Password, _emailClient, _jwtGenerator, baseUrl);
            }
            catch (InvalidPasswordException)
            {
                return new FailResponse("Your password should be longer than 5 characters.");
            }
            catch (InvalidEmailException)
            {
                return new FailResponse("The email address is invalid.");
            }
            catch (EmailAlreadyExistsException)
            {
                return new FailResponse("The email address does already exists.");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return new ErrorResponse("Something went wrong");
            }
            
            return new SuccessResponse<bool>(true);
        }

        [HttpPost("verify")]
        public JsendResponse Verify([FromBody] VerificationToken verificationToken)
        {
            var authLogic = new AuthLogic(_userDao);

            try
            {
                authLogic.VerifyUser(verificationToken.Token, _jwtGenerator);
            }
            catch (AuthenticationTokenExpiredException)
            {
                return new FailResponse("Your authentication token expired.");
            }
            catch (AuthenticationTokenInvalidException)
            {
                return new FailResponse("Your authentication token could not be verified.");
            }
            catch (UserNotFoundException)
            {
                return new FailResponse("The user that belongs to this invitation code does not exist.");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return new ErrorResponse("Something went wrong.");
            }
            
            
            return new SuccessResponse<bool>(true);
        }

        [HttpPost("authenticate")]
        public JsendResponse Authenticate([FromBody] User user)
        {
            var authLogic = new AuthLogic(_userDao);

            try
            {
                return new SuccessResponse<AuthenticationToken>(new AuthenticationToken
                {
                    AuthToken = authLogic.AuthenticateUser(user.Email, user.Password, _jwtGenerator)
                });
            }
            catch (InvalidUserCredentialsException)
            {
                return new FailResponse("The email address or password is invalid.");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return new ErrorResponse("Something went wrong.");
            }
        }
    }
}