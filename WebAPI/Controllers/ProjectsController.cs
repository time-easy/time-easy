using System;
using System.Collections.Generic;
using Logic;
using Logic.Exceptions;
using Logic.Interfaces;
using Logic.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Utils.JsendResponse;

namespace WebAPI.Controllers
{
    [Route("api/projects")]
    [ApiController]
    [Authorize]
    public class ProjectsController
    {

        private readonly ProjectLogic _projectLogic;
        
        public ProjectsController(IProjectDao projectDao)
        {
            _projectLogic = new ProjectLogic(projectDao);
        }

        [HttpPost("~/api/organization/{organizationId:int}/projects")]
        public JsendResponse AddProject([FromRoute] int organizationId, [FromBody] Project project)
        {
            //TODO: Check if the user has permission to create projects.
            try
            {
                _projectLogic.AddProject(organizationId, project.Name);
            }
            catch (OrganizationNameNullOrEmptyException)
            {
                return new FailResponse("The organization name should not be empty.");
            }
            catch (Exception e)
            {
                return new ErrorResponse("Something went wrong.");
            }
            
            return new SuccessResponse<bool>(true);
        }

        [HttpGet("~/api/organization/{organizationId:int}/projects")]
        public JsendResponse GetProjectsByOrganization([FromRoute] int organizationId)
        {
            //TODO: Check if the user has permission to get projects from this organization
            try
            {
                return new SuccessResponse<List<Project>>(_projectLogic.GetProjectsByOrganization(organizationId));
            }
            catch (Exception e)
            {
                return new FailResponse("Something went wrong.");
            }
        }
    }
}