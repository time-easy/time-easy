using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using Logic;
using Logic.DTO;
using Logic.Interfaces;
using Logic.Model;
using Logic.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Utils.JsendResponse;

namespace WebAPI.Controllers
{
    [Route("api/time")]
    [ApiController]
    [Authorize]
    public class TimeController
    {
        private readonly TimeLogic _timeLogic;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly JwtGenerator _jwtGenerator;

        public TimeController(ITimeDao timeDao, IHttpContextAccessor httpContextAccessor, JwtGenerator jwtGenerator)
        {
            this._timeLogic = new TimeLogic(timeDao);
            this._httpContextAccessor = httpContextAccessor;
            this._jwtGenerator = jwtGenerator;
        }

        [HttpPut("~/api/project/{projectId:int}/time/{day:int}/{month:int}/{year:int}")]
        public JsendResponse RegisterOwnTime([FromRoute] int projectId, [FromRoute] int day, [FromRoute] int month,
            [FromRoute] int year, [FromBody] TimeEntryInput timeEntryInput)
        {
            JwtSecurityToken jwtToken =
                (JwtSecurityToken) _jwtGenerator.DecodeJwtToken(
                    this._httpContextAccessor.HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1]);
            int userId = int.Parse(jwtToken.Claims.Single(x => x.Type == ClaimTypes.Sid).Value);
            DateTime date = new DateTime(year, month, day);
            try
            {
                this._timeLogic.RegisterTime(date, projectId, userId, timeEntryInput.Time,
                    timeEntryInput.Comment);
            }
            catch (Exception)
            {
                return new ErrorResponse("Something went wrong.");
            }

            return new SuccessResponse<bool>(true);
        }

        [HttpGet("~/api/organization/{organizationId:int}/time/{day:int}/{month:int}/{year:int}")]
        public JsendResponse GetOwnTimeByDate([FromRoute] int organizationId, [FromRoute] int day, [FromRoute] int month,
            [FromRoute] int year)
        {
            JwtSecurityToken jwtToken =
                (JwtSecurityToken) _jwtGenerator.DecodeJwtToken(
                    this._httpContextAccessor.HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1]);
            int userId = int.Parse(jwtToken.Claims.Single(x => x.Type == ClaimTypes.Sid).Value);
            DateTime date = new DateTime(year, month, day);

            return new SuccessResponse<List<TimeEntry>>(this._timeLogic.GetTimeByUserAndDateAndOrganization(userId, date, organizationId));
        }

        [HttpGet("~/api/organization/{organizationId:int}/time")]
        public JsendResponse GetAllOwnTimeEntriesByOrganization([FromRoute] int organizationId)
        {
            JwtSecurityToken jwtToken =
                (JwtSecurityToken) _jwtGenerator.DecodeJwtToken(
                    this._httpContextAccessor.HttpContext.Request.Headers["Authorization"].ToString().Split(" ")[1]);
            int userId = int.Parse(jwtToken.Claims.Single(x => x.Type == ClaimTypes.Sid).Value);

            return new SuccessResponse<List<TimeEntry>>(
                this._timeLogic.GetAllTimeEntriesByUserAndOrganization(userId, organizationId));
        }
        
        [HttpGet("~/api/organization/{organizationId:int}/member/{userId:int}")]
        public JsendResponse GetAllTimeEntriesByOrganizationAndUser([FromRoute] int organizationId, [FromRoute] int userId)
        {
            //TODO: check permissions
            return new SuccessResponse<List<TimeEntry>>(
                this._timeLogic.GetAllTimeEntriesByUserAndOrganization(userId, organizationId));
        }
    }
}