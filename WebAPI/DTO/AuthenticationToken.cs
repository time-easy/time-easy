namespace WebAPI.DTO
{
    public class AuthenticationToken
    {
        public string AuthToken { get; set; }
    }
}