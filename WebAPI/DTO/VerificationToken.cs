namespace WebAPI.DTO
{
    public class VerificationToken
    {
        public string Token { get; set; }
    }
}