using System.Net;
using System.Net.Mail;
using Logic.Interfaces;

namespace WebAPI.Utils
{
    public class EmailClient : IEmailClient
    {
        private readonly SmtpClient _smtpClient;
        private readonly string _fromEmail;
        public EmailClient(string host, int port, string username, string password)
        {
            _fromEmail = username;

            _smtpClient = new SmtpClient
            {
                Port = port,
                Host = host,
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(username, password),
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
        }

        public void SendMail(string receiver, string subject, string message)
        {
            using (MailMessage mailMessage = new MailMessage()
            {
                From = new MailAddress(_fromEmail),
                Subject = subject,
                IsBodyHtml = true,
                Body = message
            })
            {
                mailMessage.To.Add(new MailAddress(receiver));
                _smtpClient.Send(mailMessage);
            }
        }
    }
}