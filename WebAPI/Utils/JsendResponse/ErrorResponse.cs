namespace WebAPI.Utils.JsendResponse
{
    public class ErrorResponse : JsendResponse
    {
        public string Message { get; }

        public ErrorResponse(string message)
        {
            base.ResponseType = ResponseType.Error;
            Message = message;
        }
    }
}