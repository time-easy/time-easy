using System.Runtime.Serialization;

namespace WebAPI.Utils.JsendResponse
{
    public enum ResponseType
    {
        [EnumMember(Value = "success")]
        Success,
        [EnumMember(Value = "fail")]
        Fail,
        [EnumMember(Value = "error")]
        Error
    }
}