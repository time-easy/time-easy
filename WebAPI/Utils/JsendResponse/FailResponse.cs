namespace WebAPI.Utils.JsendResponse
{
    public class FailResponse : JsendResponse
    {
        public string Message { get; }

        public FailResponse(string message)
        {
            base.ResponseType = ResponseType.Fail;
            Message = message;
        }
    }
}