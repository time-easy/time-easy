using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WebAPI.Utils.JsendResponse
{
    public abstract class JsendResponse
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ResponseType ResponseType { get; set; }
    }
}