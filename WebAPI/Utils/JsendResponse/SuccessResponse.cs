namespace WebAPI.Utils.JsendResponse
{
    public class SuccessResponse<T> : JsendResponse
    {
        public T Data { get; }

        public SuccessResponse(T data)
        {
            base.ResponseType = ResponseType.Success;
            Data = data;
        }
    }
}